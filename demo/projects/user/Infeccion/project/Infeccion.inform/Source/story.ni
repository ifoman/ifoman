"Infección" by Shadow de Coutemeier.

Release along with an interpreter.
Include Spanish Language by Sebastian Arg.

The story description is "Una simple demostración para ifoman, en dos habitaciones de tu casa.".
The story creation year is 2018.
The story headline is "Una historia de prueba".
The story genre is "Science Fiction".

La cocina is a room. "Una bonita cocina, creo que es la tuya. ¿Me equivoco?  Al norte, ves el salón.".
El afilado cuchillo is a thing in la cocina. The description is "Un reluciente y afilado cuchillo, que te hará pupita si te descuidas".

Instead of taking el afilado cuchillo:
	say "Torpemente intentas coger el cuchillo con tus manos, pero son de mantequilla, se te cae y se clava en tu pie.";
	say "¡Insensato! Te advertí que te haría pupita.";
	end the story saying "Aquí se acaba esta mini aventura.".

El salón is a room. "Ah, el remanso de paz de cualquier enbrutecido humano.".
La televisión is a thing in el salón. The description is "Un pedazo de tele de 80 pulgadas, sonido cuadrafónico, 8 puertos USB, WIFI, Ethernet, disco duro integrado de tropecientos gigas y no sé que más barbaridades.".
Instead of taking la televisión:
	say "No, hombre, ¿por qué ibas a cargar con ella de un lado a otro?".


El salón is north of La cocina.