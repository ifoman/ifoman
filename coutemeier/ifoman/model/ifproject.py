import io, os, random

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0

from . import status
from .inform7 import inform7

class IFProject:
    def __init__(self, owner, path):
        self.id = str(random.randint(0, 10000000))
        self.name = os.path.basename(path)
        self.owner = owner
        self.path = path
        # This is the folder where is stored the files of the project type
        self.projectPath = os.path.join(self.path, 'project')
        self.settingsFilePath = os.path.join(self.path, 'Settings.ini')
        self.builder = None
        self.tags = []
        # Vars from settings
        self.autodetect()

    def reload(self):
        self.tags = []
        self.reloadSettings()
        self.reloadSettings()

    def reloadSettings(self):
        config = ConfigParser()
        if (os.path.exists(self.settingsFilePath)):
            with io.open(self.settingsFilePath, encoding='utf-8', errors='ignore', mode='r') as f:
                config.readfp(f)

    """Tries to guess the if adventure.

    For the moment is manually
    """
    def autodetect(self):
        self.adv = inform7.create(self)
        # if self.adventure is None...

    def build(self):
        return self.builder.build(self)

