import os, re

import config
from ..adventure import Adventure
from ..builders.inform7builder import Inform7Builder

def appliesTo(project):
    return os.path.exists(os.path.join(project.projectPath, project.name + '.inform'))

def create(project):
    if not appliesTo(project):
        return None
    return Inform7Adventure(project)

class Inform7Adventure(Adventure):
    COVER_FILENAMES = ['Small Cover.jpg', 'Small Cover.png', 'Cover.jpg', 'Cover.png']
    STORY_TITLE_AUTHOR_LANGUAGE_RE = r'"(.*?)"(?:\s+by\s+([\w ]+))?\s*?(?:\(in\s+(.*?)\))?'
    TITLE_RE = r'The\s+story\s+title\s+is\s+\"([^\"]+)\"'
    AUTHOR_RE = r'The\s+story\s+author\s+is\s+\"([^\"]+)\"'
    HEADLINE_RE = r'The\s+story\s+headline\s+is\s+\"([^\"]+)\"'
    GENRE_RE = r'The\s+story\s+genre\s+is\s+\"([^\"]+)\"'
    RELEASE_NUMBER_RE = r'The\s+release\s+number\s+is\s+([^\.]+)'
    DESCRIPTION_RE = r'The\s+story\s+description\s+is\s+\"([^\"]+)\"'
    CREATION_YEAR_RE = r'The\s+story\s+creation\s+year\s+is\s+([^\.]+)'

    def __init__(self, project):
        Adventure.__init__(self, project)
        self.informPath = os.path.join(project.projectPath, project.name + '.inform')
        self.materialsPath = os.path.join(project.projectPath, project.name + '.materials')
        self.sourcePath = os.path.join(self.informPath, 'Source', 'story.ni')
        self.zblorbjs = os.path.join(self.materialsPath, 'Release', 'interpreter', self.project.name + '.zblorb.js')
        project.builder = Inform7Builder()
        self.reload()

    def reloadAdventure(self):
        self.updateCoverPath()
        self.readBibliography()
        self.uuid = self.readUUID()

    def readUUID(self):
        uuidPath = os.path.join(self.informPath, 'uuid.txt')
        with open(uuidPath, "r") as f:
            #return f.readlines()
            return f.read().decode("UTF-8")

    def readBibliography(self):
        lines = ''
        with open(self.sourcePath, "r") as f:
            for line in f:
                lines += line
                if (line.startswith('Section ')):
                    break

        matchObj = re.search(Inform7Adventure.STORY_TITLE_AUTHOR_LANGUAGE_RE, lines, re.M|re.I)
        if (matchObj):
            self.title = matchObj.group(1)
            self.author = matchObj.group(2)
            self.language = matchObj.group(3)

        self.m(Inform7Adventure.TITLE_RE, lines, 'title')
        self.m(Inform7Adventure.AUTHOR_RE, lines, 'author')
        self.m(Inform7Adventure.GENRE_RE, lines, 'genre')
        self.m(Inform7Adventure.HEADLINE_RE, lines, 'headline')
        self.m(Inform7Adventure.RELEASE_NUMBER_RE, lines, 'releaseNumber')
        self.m(Inform7Adventure.DESCRIPTION_RE, lines, 'description')
        self.m(Inform7Adventure.CREATION_YEAR_RE, lines, 'creationYear')

    def updateCoverPath(self):
        self.cover = ''
        self.coverUrl = '/static/img/empty.jpg'
        for filename in Inform7Adventure.COVER_FILENAMES:
            fullFilename = os.path.join(self.materialsPath, filename)
            if os.path.isfile(fullFilename):
                self.cover = fullFilename
                self.coverUrl = '/myprojects/cover/' + self.project.id
                break

    def m(self, regex, lines, attribute):
        matchObj = re.search(regex, lines, re.M|re.I|re.U)
        if (matchObj):
            setattr(self, attribute, matchObj.group(1))
