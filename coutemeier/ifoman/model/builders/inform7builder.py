from .builder import Builder

class Inform7Builder(Builder):
    def __init__(self):
        Builder.__init__(self)
        self.name = 'INFORM7'

    def build(self, project, release = False):
        return self.call(
            args = ['/usr/local/bin/i7', '-r' if release else '-c', project.adv.informPath],
            startInPath = project.path
        )
