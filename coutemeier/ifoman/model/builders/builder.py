import subprocess
from subprocess import check_output, Popen

class Builder():
    def __init__(self):
        self.name = 'UNKNOWN'

    def build(self, project, release):
        pass

    def callInBackground(self, args, startInPath):
        return check_output(args, universal_newlines=True).decode('utf-8')

    def call(self, args, startInPath):
        pipe = Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, close_fds=True, cwd=startInPath)
        out, err = pipe.communicate()
        return out.decode('utf-8')

