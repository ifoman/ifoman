class Adventure:
    def __init__(self, project):
        self.project = project
        self.cover = None
        self.coverUrl = None
        self.author = ''
        self.creationYear = ''
        self.description = ''
        self.postTitle = None
        self.genre = ''
        self.headline = ''
        self.language = ''
        self.releaseNumber = ''
        self.title = ''
        self.uuid = ''
        self.tool = ''

    def reload(self):
        self.project.tags = [ self.project.builder.name ]
        self.postTitle = None
        self.reloadAdventure()
        if self.author: self.postTitle = ( self.headline + ', ' if self.headline else '' ) + 'by ' + self.author
        if self.creationYear: self.postTitle = ( self.postTitle + ' ' if self.postTitle else '') + "({})".format(self.creationYear)
        if self.genre:
            self.project.tags.append(self.genre)
        if self.creationYear:
            self.project.tags.append(self.creationYear)
