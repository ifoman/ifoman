class Status:
    def __init__(self, status, css, text):
        self.status = status
        self.css = css
        self.test = text



Status.UNKNOWN = Status(-1, 'far fa-question-square bg-secondary text-white', 'UNKNOWN')
Status.ERROR   = Status(90, 'far fa-exclamation-square bg-danger text-white', 'ERROR')
Status.SUCCESS = Status(100, 'far fa-check-square bg-success text-white', 'SUCCESS')

Status.STATUS = {
    Status.UNKNOWN.status : Status.UNKNOWN,
    Status.ERROR.status : Status.ERROR,
    Status.SUCCESS.status : Status.SUCCESS
}


def valueOf(status):
    return Status.STATUS.get(status, Status.UNKNOWN)