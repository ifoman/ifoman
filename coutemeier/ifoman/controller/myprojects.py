import io, os, sys, uuid, web
from shutil import copyfile
from web import ctx, form


import config
from ...core.web.security.actions import BasicAction, userRoleRequired
from ..model.ifproject import IFProject

urls = (
    '/', 'List',
    '/build/([^\\/]+)', 'Build',
    '/cover/([^\\/]+)', 'Cover',
    '/create', 'Create',
    '/import', 'Import',
    '/list', 'List',
    '/project/([^\\/]+)', 'ProjectView',
    '/run/([^\\/]+)', 'Run',
    '/edit/([^\\/]+)', 'Edit',
    '/zblorbjs/([^\\/]+).zblorb.js', 'ZBlorbJS'
)

app = web.application(urls, globals())
render = web.template.render('templates/', globals(), base='layout')

class List(object):
    @userRoleRequired
    def GET(self):
        projects = ctx.session.myprojects
        if projects is None:
            owner = ctx.session.user.username
            userPath = os.path.join(config.projectsPath, owner)
            projects = {}
            if (os.path.isdir(userPath)):
                for dir in next(os.walk(userPath))[1]:
                    if_project = IFProject(owner, os.path.join(userPath, dir))
                    if not if_project is None:
                        projects[if_project.id] = if_project
            ctx.session.myprojects = projects
        # Sort the list of projects by title
        projectList=projects.values()
        projectList.sort(key=lambda p: p.adv.title, reverse=False)
        return render.myprojects.list(projectList)

class Create(object):
    @userRoleRequired
    def GET(self):
        #render = web.template.render('templates/', globals={ 'session': ctx.session }, base='layout')
        #render = web.template.render('templates/', globals(), base='layout')
        return render.myprojects.create()

    @userRoleRequired
    def POST(self):
        form = web.input()
        projectName = form.projectName
        if form.get('action') == 'cancel':
            return web.seeother('/')

        userProjectsPath = web.ctx.session.user.i7settings.myprojectsPath
        informProjectFolder = os.path.join(userProjectsPath, projectName, projectName + '.inform')
        projectFolders = [
            os.path.join(userProjectsPath, projectName, projectName + '.materials', 'Release'),
            os.path.join(informProjectFolder, 'Build'),
            os.path.join(informProjectFolder, 'Index'),
            os.path.join(informProjectFolder, 'Source')
        ]
        for projectFolder in projectFolders:
            os.makedirs(projectFolder)

        projectUUID = uuid.uuid4()
        f=open(os.path.join(informProjectFolder, 'uuid.txt'), 'w')
        f.write(str(projectUUID))
        f.write('\n')
        f.close()
        f = open(os.path.join(informProjectFolder, 'Source', 'story.ni'), 'w')
        f.write('"%s" by %s.\n\n' % (form.projectTitle, web.ctx.session.user.username))
        if (form.storyGenre):
            f.write('The story genre is "%s".\n' % (form.storyGenre))
        if (form.storyHeadline):
            f.write('The story headline is "%s".\n' % (form.storyHeadline))
        if (form.storyDescription):
            f.write('The story description is "%s".\n\n ' % (form.storyDescription))
        f.write('The kitchen is a room. The description is "The kitchen of my house.".')
        f.close()
        # Create the Settings.plist (glulx for the moment)
        copyfile(os.path.join(config.resourcesProjectTemplates, config.i7BuildFormats['glulx']), os.path.join(informProjectFolder, config.i7SettingsFile))
        #render = web.template.render('templates/', globals(), base='layout')
        ctx.session.myprojects = None
        return render.myprojects.created(form)

class Cover(object):
    @userRoleRequired
    def GET(self, id):
        project = ctx.session.myprojects.get(id)
        web.header('Content-type', 'image/jpg')
        return open(project.adv.cover, 'rb').read()

class Build(BasicAction):
    @userRoleRequired
    def GET(self, id):
        project = ctx.session.myprojects.get(id)
        if project is None:
            return render.myprojects.errors.not_found(id)
        output = project.build()
        return render.myprojects.project(project, output)

class Run(BasicAction):
    @userRoleRequired
    def GET(self, id):
        project = ctx.session.myprojects.get(id)
        if project is None:
            return render.myprojects.errors.not_found(id)
        if os.path.isfile(project.adv.zblorbjs):
            return render.myprojects.run(project)
        return render.errors.generic('You need to build the adventure first.')

class Edit(BasicAction):
    @userRoleRequired
    def GET(self, id):
        return self.functionalityNotYetAvailable()

class Import(BasicAction):
    @userRoleRequired
    def GET(self):
        return self.functionalityNotYetAvailable()

class ProjectView(BasicAction):
    @userRoleRequired
    def GET(self, id):
        project = ctx.session.myprojects.get(id)
        if project is None:
            return render.myprojects.errors.not_found(id)
        return render.myprojects.project(project, None)

class ZBlorbJS(BasicAction):
    @userRoleRequired
    def GET(self, id):
        project = ctx.session.myprojects.get(id)
        if project is None:
            return render.myprojects.errors.not_found(id)
        web.header('Content-type', 'application/javascript')
        return open(project.adv.zblorbjs, 'rb').read()

class Inform7(object):
    def __init__(self, username):
        self.username = username
        self.userPath = os.path.join(config.projectsPath, username)

    def list(self):
        projects = {}
        if (os.path.isdir(self.userPath)):
            for dir in next(os.walk(self.userPath))[1]:
                if_project = IFProject(self.username, os.path.join(self.userPath, dir))
                if not if_project is None:
                    projects[if_project.id] = if_project
        return projects
