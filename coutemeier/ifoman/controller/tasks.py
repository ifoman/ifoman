import web

from ...core.web.security.actions import BasicAction, adminRoleRequired, userRoleRequired

urls = (
    '/', 'list',
    '/list', 'list',
    '/running', 'listRunning',
    '/cancelled', 'listCancelled',
    '/pending', 'listPending',
    '/finished', 'listFinished'
)

app = web.application(urls, globals())
render = web.template.render('templates/', globals(), base='layout')

class list(BasicAction):
    @adminRoleRequired
    def GET(self):
        return self.functionalityNotYetAvailable()

class listRunning(BasicAction):
    @adminRoleRequired
    def GET(self):
        return self.functionalityNotYetAvailable()

class listCancelled(BasicAction):
    @adminRoleRequired
    def GET(self):
        return self.functionalityNotYetAvailable()


class listPending(BasicAction):
    @adminRoleRequired
    def GET(self):
        return self.functionalityNotYetAvailable()

class listFinished(BasicAction):
    @adminRoleRequired
    def GET(self):
        return self.functionalityNotYetAvailable()
