import web
from ...core.web.security.actions import BasicAction, userRoleRequired

urls = (
    '/list', 'list',
    '/search', 'search',
    '/cover/([^\\/]+)/([^\\/]+)', 'cover'
)

app = web.application(urls, globals())
render = web.template.render('templates/', globals(), base='layout')

class list(BasicAction):
    def GET(self):
        return self.functionalityNotYetAvailable()

class search(BasicAction):
    def GET(self):
        return self.functionalityNotYetAvailable()
