import logging, threading, time
from concurrent import futures
import config

class BuildManager():
    def __init__(self, loggingManager):
        self.logger = loggingManager.getLogger(__name__)
        self.logger.info('Clase BuildManager.logging creada')
        self.tasks = []
        self.manager = futures.ThreadPoolExecutor(max_workers = config.building_threads)
        self.counter = 0
        self.cancelledCounter = 0
        self.pendingCounter = 0
        self.runningCounter = 0
        self.doneCounter = 0
        self.lock = threading.Lock()
        self.cancelled = False
        self.refreshCounters()

    def refreshCounters(self):
        self.logger.info('Refrescando contadores')
        with self.lock:
            self.counter = 0
            self.cancelledCounter = 0
            self.pendingCounter = 0
            self.runningCounter = 0
            self.doneCounter = 0
            for task in self.tasks:
                self.counter += 1
                if task.running():
                    self.runningCounter += 1
                elif task.cancelled():
                    self.cancelledCounter += 1
                elif task.done():
                    self.doneCounter += 1
                else:
                    self.pendingCounter += 1
        if not self.cancelled: threading.Timer(60.0, self.refreshCounters).start()

    def submit(self, task):
        # TODO pendiente
        self.logger.info('submit Anhadiendo tarea')
        with self.lock:
            theWorker = self.manager.submit(task)
            self.tasks.append(theWorker)
        self.logger.info('submit Tarea anhadida')

    def cancel(self):
        self.cancelled = True
