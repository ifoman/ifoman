import logging, logging.config

class LoggingManager:
    def __init__(self, initialMessage):
        logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
        #logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
        self.logger = logging.getLogger(__name__)
        self.logger.info(initialMessage)

    def getLogger(self, name):
        return logging.getLogger(name)
