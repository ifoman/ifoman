import traceback
import web
from web import ctx
from ..security.actions import adminRoleRequired, userRoleRequired
from .user import User
from .service import UserLocator

urls = (
    '/signin', 'signin',
    '/signout', 'signout',
    '/profile/([^\\/]+)', 'profile',
    '/myprofile', 'myprofile'
)

app = web.application(urls, globals())
render = web.template.render(
    'templates/', globals(), base='layout'
)

class signout:
    def GET(self):
        ctx.session.kill()
        return web.seeother('/', absolute=True)

class signin:
    def GET(self):
        return render.users.signin()

    def POST(self):
        form = web.input()
        userExists = False
        if UserLocator.arePasswordsEquals(form.username, form.password):
            try:
                theuser = UserLocator.findByUsername(form.username)
                ctx.session.user = theuser
                ctx.session.roles = theuser.roles
                ctx.session.logged_in = True
                return web.seeother('/signedin', absolute = True)
            except:
                print "User not valid"

        return render.users.loginfailed('Those login details don\'t work.' + traceback.format_exc())

class profile():
    @userRoleRequired
    def GET(self, id):
        return render.users.profile(ctx.session.user)

class myprofile():
    @userRoleRequired
    def GET(self):
        return web.seeother('profile/{}'.format(ctx.session.user.id))
