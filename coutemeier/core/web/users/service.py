try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0

import os

import config
from .user import User

class UserLocator(object):
    @staticmethod
    def findByUsername(username):
        userConfig = UserLocator.readProfile(username)
        if userConfig.has_option('Security', 'password'):
            return User(
                displayName = userConfig.get('Personal', 'display_name'),
                id = userConfig.get('Security', 'id'),
                username = username,
                roles = userConfig.get('Security', 'roles').split(',')
            )
        raise Exception("Username/Password doesn't match or exists")

    @staticmethod
    def arePasswordsEquals(username, password):
        userConfig = UserLocator.readProfile(username)
        if userConfig.has_option('Security', 'password'):
            profilePassword = userConfig.get('Security', 'password')
            saltedPassword = password
            return saltedPassword == profilePassword
        return False

    @staticmethod
    def readProfile(username):
        userConfig = ConfigParser()
        profilePath = os.path.join(config.usersPath, username + '.ini')
        try:
            if (os.path.exists(profilePath)):
                userConfig.read(profilePath)
        except:
            pass
        return userConfig
