from web import ctx

class User:
    def __init__(self, id, username, displayName, roles = []):
        self.id = id
        self.username = username
        self.displayName = displayName
        self.anonymous = (username == 'anonymous')
        self.roles = roles
        # By default.
        self.logged_in = False

User.ANONYMOUS_USER = User(42987498742198, 'anonymous', 'Anonymous user', 'anonymous')
