import web
from web import ctx

render = web.template.render('templates/', globals(), base='layout')

"""The most basic web action.

It represents an elemental action that doesn't required any role.
It provides some methods do simplify some common problems, for example,
rendering a page that shows that an action is not yet implemented.
"""
class BasicAction:
    def GET(self):
        return self.functionalityNotYetAvailable()

    def POST(self):
        return self.functionalityNotYetAvailable()

    def forbidden(self):
        return render.errors.http_403()

    """Convenient method for showing a page that inform to the user that the action is not yet available."""
    def functionalityNotYetAvailable(self):
        return render.errors.http_501()

def roleRequired(role):
    def roleRequired_wrapper(func):
        def action_wrapper(*args, **kwargs):
            if not ctx.session.logged_in:
                return web.seeother('/users/signin', absolute = True)
            if not role in ctx.session.roles:
                return render.errors.http_403()
            return func(*args, **kwargs)
        return roleRequired_wrapper

def adminRoleRequired(func):
    def adminRoleRequired_wrapper(*args, **kwargs):
        if not ctx.session.logged_in:
            return web.seeother('/users/signin', absolute=True)
        if not 'admin' in ctx.session.roles:
            return render.errors.http_403()
        return func(*args, **kwargs)
    return adminRoleRequired_wrapper

def userRoleRequired(func):
    def userRoleRequired_wrapper(*args, **kwargs):
        if not ctx.session.logged_in:
            return web.seeother('/users/signin', absolute=True)
        if not 'user' in ctx.session.roles:
            return render.errors.http_403()
        return func(*args, **kwargs)
    return userRoleRequired_wrapper
