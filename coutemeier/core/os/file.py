import os, uuid

def createRandomDirs(inPath):
    while True:
        newFolder = os.path.join(inPath, uuid.uuid4().hex)
        try:
            os.makedirs(newFolder)
            return newFolder
        except os.OSError as e:
            pass
