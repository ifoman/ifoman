import os

MKDIR_CHMOD = 0750

# Current app path
appPath = os.getcwd()

# If you doesn't specify an INFOMAN_HOME environment variable, this should be the home path.
defaultHomePath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'demo')
# Root folder for ifoman persistence
homePath = os.getenv('IFOMAN_HOME', defaultHomePath)
# Where valid users are stored
usersPath = os.path.join(homePath, 'users')
# Where projects are stored
projectsPath = os.path.join(homePath, 'projects')
# The folder where sessions will be persisted
sessionPath = os.path.join(homePath, 'sessions')

if not os.path.exists(homePath): os.makedirs(homePath, MKDIR_CHMOD)
if not os.path.exists(usersPath): os.makedirs(usersPath, MKDIR_CHMOD)
if not os.path.exists(projectsPath): os.makedirs(projectsPath, MKDIR_CHMOD)

# Resources out of the box, such as empty image.
resourcesPath = os.path.join(appPath, 'resources')
imagesPath = os.path.join(resourcesPath, 'img')
resourcesTemplates = os.path.join(resourcesPath, 'templates')
resourcesProjectTemplates = os.path.join(resourcesTemplates, 'projects')

IFOMAN_VERSION = '0.0.1-SNAPSHOT'
building_threads = 5

# Once used, don't change it, or other passwords won't work anymore.
passwords_salt = 'My secret'

# Specific for Inform 7. I would move them as soon as possible.
i7SettingsFile = 'Settings.plist'
i7BuildFormats = { 'glulx' : 'glulx.plist' }
