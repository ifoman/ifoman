import web

import config

from coutemeier.ifoman.controller import myprojects
from coutemeier.ifoman.controller import projects
from coutemeier.ifoman.controller import tasks
from coutemeier.core.web.users import users

from coutemeier.core.concurrent import buildmanager
from coutemeier.core.logging.loggingmanager import LoggingManager

import coutemeier.core.web.users.users
from coutemeier.core.web.users.user import User

from coutemeier.core.web.security.actions import adminRoleRequired

web.config.debug = True
LOGGING_MANAGER = LoggingManager('Application started up')

# TODO Incorrect way to manage a buildManager per user
BUILD_MANAGERS_BY_USER = {}

class Welcome(object):
    def GET(self):
        return RENDER.welcome(' ')

class SignedIn(object):
    def GET(self):
        return web.seeother('/myprojects/list')

class Configuration(object):
    @adminRoleRequired
    def GET(self):
        return RENDER.configuration.index()

def session_created_hook():
    web.ctx.session = session
    web.template.Template.globals['config'] = config
    web.template.Template.globals['session'] = session
    web.template.Template.globals['hasattr'] = hasattr
    #user_build_manager = buildmanager.BuildManager(LOGGING_MANAGER)
 #   BUILD_MANAGERS_BY_USER[session] = user_build_manager
 #   web.template.Template.globals['buildmanager'] = user_build_manager

def session_destroyed_hook():
#    buildmanager = web.template.Template.globals['buildmanager']
#    web.template.Template.globals['buildmanager'] = None
#    buildmanager.cancel()
    web.ctx.session = None

ROUTES = (
    '/', 'Welcome',
    '/configuration', 'Configuration',
    '/index', 'Welcome',
    '/signedin', 'SignedIn',
    '/myprojects', myprojects.app,
    '/projects', projects.app,
    '/tasks', tasks.app,
    '/users', users.app
)

APP = web.application(ROUTES, globals())
session = web.session.Session(
    APP,
    web.session.DiskStore(config.sessionPath),
    initializer={
        'logged_in': False,
        'roles' : ['anonymous'],
        'user': User.ANONYMOUS_USER,
        'myprojects' : None
    }
)
web.config.session_parameters['cookie_name'] = 'ifoman_id'
web.config.session_parameters['cookie_domain'] = None
web.config.session_parameters['timeout'] = 1800, #24 * 60 * 60, # 24 hours   in seconds
web.config.session_parameters['ignore_expiry'] = True
web.config.session_parameters['ignore_change_ip'] = True
web.config.session_parameters['secret_key'] = 'fdasLKJFDASfnemwlkawefnki'
web.config.session_parameters['expired_message'] = 'Session expired'
RENDER = web.template.render('templates/', globals(), base='layout')

APP.add_processor(web.loadhook(session_created_hook))
APP.add_processor(web.unloadhook(session_destroyed_hook))
if __name__ == "__main__":
    APP.run()
